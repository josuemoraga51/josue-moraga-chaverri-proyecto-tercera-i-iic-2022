"use strict";

console.log('Cargando hamburger icon...');

(function () {
  var MAIN_OBJ = {
    init: function init() {
      this.eventHandlers();
    },
    eventHandlers: function eventHandlers() {
      document.querySelector('.hamburger-icon').addEventListener('click', function () {
        document.querySelector('.menu-container').classList.toggle('menu-open');
      });
      document.querySelector('sub-hamburger-icon').addEventListener('click', function () {
        document.querySelector('.sub-menu-container').classList.toggle('sub-menu-open');
      });
    }
  };
  MAIN_OBJ.init();
})();
//# sourceMappingURL=hamburger.icon.dev.js.map
