console.log('Cargando Hamburger Icon...');
(function () {
    const MAIN_OBJ = {
        init: function () {
            this.eventHandlers()
        },
        eventHandlers: function () {
            document.querySelector('.hamburger-icon').addEventListener('click', function(){
                document.querySelector('.menu-container').classList.toggle('menu-open');
            });
            document.querySelector('.sub-hamburger-icon').addEventListener('click', function(){
                document.querySelector('.sub-menu-container').classList.toggle('sub-menu-open');
            });
        }
    }
    MAIN_OBJ.init();
})();