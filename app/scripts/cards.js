console.log('Cargando Cards...');
const dataCards = [
    {
        "title": "Computadoras",
        "url_image": "https://www.elgrupoinformatico.com/static/Manuales/2016/01/monitor-16-9-260116.jpg",
        "desc": "Muchas veces algo se pone de moda, todos lo probamos y, hasta unos años después de que pasa la expectación inicial, no nos damos cuenta de que la idea no era tan buena. Ahora nos vamos a preguntar si el formato 16:9 es el mejor para las pantallas de ordenador. Obviamente, en tablets ya ha quedado claro que no siempre resulta adecuado, pero ahora profundizaremos un poco más en este asunto.",
        "cta": "Mostrar más...",
        "link": "https://www.elgrupoinformatico.com/formato-mejor-para-las-pantallas-ordenador-t27869.html"
    },
    {
        "title": "Computadoras",
        "url_image": "https://cloudfront-us-east-1.images.arcpublishing.com/infobae/4IK3K6EJ2NBNJFM4J5OSJBJT7Q.jpg",
        "desc": "Muchas veces algo se pone de moda, todos lo probamos y, hasta unos años después de que pasa la expectación inicial, no nos damos cuenta de que la idea no era tan buena. Ahora nos vamos a preguntar si el formato 16:9 es el mejor para las pantallas de ordenador. Obviamente, en tablets ya ha quedado claro que no siempre resulta adecuado, pero ahora profundizaremos un poco más en este asunto.",
        "cta": "Mostrar más...",
        "link": "https://www.elgrupoinformatico.com/formato-mejor-para-las-pantallas-ordenador-t27869.html"
    },
    {
        "title": "Computadoras",
        "url_image": "https://www.tecnologia-informatica.com/wp-content/uploads/2018/08/caracteristicas-de-las-computadoras-1.jpeg",
        "desc": "Muchas veces algo se pone de moda, todos lo probamos y, hasta unos años después de que pasa la expectación inicial, no nos damos cuenta de que la idea no era tan buena. Ahora nos vamos a preguntar si el formato 16:9 es el mejor para las pantallas de ordenador. Obviamente, en tablets ya ha quedado claro que no siempre resulta adecuado, pero ahora profundizaremos un poco más en este asunto.",
        "cta": "Mostrar más...",
        "link": "https://www.elgrupoinformatico.com/formato-mejor-para-las-pantallas-ordenador-t27869.html"
    },
    {
        "title": "Computadoras",
        "url_image": "https://uvn-brightspot.s3.amazonaws.com/assets/vixes/btg/curiosidades.batanga.com/files/Historia-de-la-computadora-5_0.jpg",
        "desc": "Muchas veces algo se pone de moda, todos lo probamos y, hasta unos años después de que pasa la expectación inicial, no nos damos cuenta de que la idea no era tan buena. Ahora nos vamos a preguntar si el formato 16:9 es el mejor para las pantallas de ordenador. Obviamente, en tablets ya ha quedado claro que no siempre resulta adecuado, pero ahora profundizaremos un poco más en este asunto.",
        "cta": "Mostrar más...",
        "link": "https://www.elgrupoinformatico.com/formato-mejor-para-las-pantallas-ordenador-t27869.html"
    },
    {
        "title": "Computadoras",
        "url_image": "https://sistemas.com/wp-content/uploads/ayudas/Recomendaciones-computadora.jpg",
        "desc": "Muchas veces algo se pone de moda, todos lo probamos y, hasta unos años después de que pasa la expectación inicial, no nos damos cuenta de que la idea no era tan buena. Ahora nos vamos a preguntar si el formato 16:9 es el mejor para las pantallas de ordenador. Obviamente, en tablets ya ha quedado claro que no siempre resulta adecuado, pero ahora profundizaremos un poco más en este asunto.",
        "cta": "Mostrar más...",
        "link": "https://www.elgrupoinformatico.com/formato-mejor-para-las-pantallas-ordenador-t27869.html"
    },
    {
        "title": "Computadoras",
        "url_image": "https://www.reviewbox.com.mx/wp-content/uploads/2020/02/niclas-illg-zEBqF_E2FIY-unsplash-scaled.jpg",
        "desc": "Muchas veces algo se pone de moda, todos lo probamos y, hasta unos años después de que pasa la expectación inicial, no nos damos cuenta de que la idea no era tan buena. Ahora nos vamos a preguntar si el formato 16:9 es el mejor para las pantallas de ordenador. Obviamente, en tablets ya ha quedado claro que no siempre resulta adecuado, pero ahora profundizaremos un poco más en este asunto.",
        "cta": "Mostrar más...",
        "link": "https://www.elgrupoinformatico.com/formato-mejor-para-las-pantallas-ordenador-t27869.html"
    }
];

(function () {
    let CARD = {
        init: function () {
            console.log('El módulo de carga funciona correctamente');
            let _self = this;
            //llamamos las funciones
            this.insertData(_self);
        },

        insertData: function (_self) {
            dataCards.map(function (item, index) {
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
            });
        },

        tplCardItem: function (item, index) {
            return(`<div class='card-item' id="card-number-${index}">
            <img src="${item.url_image}"/>
            <div class='card-info'>
                <p class='card-title'>${item.title}</p>
                <p class='card-desc'>${item.desc}</p>
                <a class='card-cta' target='blank' href="${item.link}">${item.cta}</a>
            </div>
            </div>`)
        },
    }
    CARD.init();
})();

